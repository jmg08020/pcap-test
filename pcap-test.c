#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include "pcap-test.h"

void usage() 
{
	printf("syntax: pcap-test <interface>\n");
	printf("sample: pcap-test wlan0\n");
}

typedef struct 
{
	char* dev_;
} Param;

Param param  = {
	.dev_ = NULL
};

bool parse(Param* param, int argc, char* argv[]) {
	if (argc != 2) {
		usage();
		return false;
	}
	param->dev_ = argv[1];
	return true;
}

int main(int argc, char* argv[]) {
	if (!parse(&param, argc, argv))
		return -1;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
		return -1;
	}

	while (true) {		

		struct libnet_ethernet_hdr* ethernet;	
		struct libnet_ipv4_hdr* ip;			
		struct libnet_tcp_hdr* tcp;	

		struct pcap_pkthdr* header;		
		const u_char* packet;
		int res = pcap_next_ex(pcap, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}
		
		int offset = 0;	
		int len = 0;
		ethernet = (struct libnet_ethernet_hdr*)packet;		
		len = 14;
		packet += 14;
		u_int16_t eth_type = ntohs(ethernet->ether_type);
		if(eth_type != 0x0800)		
		{
			continue;
		}	
		ip = (struct libnet_ipv4_hdr*)packet;
		offset = ip->ip_hl * 4;	
		packet += offset;
		len += offset;
		if(ip->ip_p != IPPROTO_TCP)		
		{
			continue;
		}
		tcp = (struct libnet_tcp_hdr*)packet;
		offset = tcp->th_off * 4;	
		packet += offset;
		len += offset;

		printf("==================================================\n\n");
		printf("               Ethernet header          \n");
		printf("MAC: %2x:%2x:%2x:%2x:%2x:%2x", ethernet->ether_dhost[0], ethernet->ether_dhost[1], ethernet->ether_dhost[2], ethernet->ether_dhost[3], ethernet->ether_dhost[4], ethernet->ether_dhost[5]);
		printf("-> %2x:%2x:%2x:%2x:%2x:%2x\n", ethernet->ether_shost[0], ethernet->ether_shost[1], ethernet->ether_shost[2], ethernet->ether_shost[3], ethernet->ether_shost[4], ethernet->ether_shost[5]);
		printf("                   IP header               \n");
		printf("IP:PORT : %s:%d",inet_ntoa(ip->ip_src), ntohs(tcp->th_sport));
		printf("-> %s:%d\n",inet_ntoa(ip->ip_dst), ntohs(tcp->th_dport));	
        
		if(len >= header->caplen)	
		{
			printf("                   No Data               \n");
		}
		else
		{	
			int i = 0;
			printf("                Pay load(Data)               \n");
			printf("Data : ");
			while (i < 10)
			{
				printf("%02x ", packet[i]);
				i++;
			}		
		}
		printf("\n");
		printf("==================================================\n");
	}
	pcap_close(pcap);
}
